#!/usr/bin/env python3
import os
from aws_cdk import core as cdk
from aws_cdk import core

from Project_Script.glue_database import GlueDatabaseStack
from Project_Script.glue_crawler import GlueCrawlerStack
from Project_Script.glue_job import GlueJobStack
from Project_Script.glue_lambda import GlueLambdaStack
from Project_Script.glue_workflow import GlueWorkflowStack


app = core.App()

GlueDatabaseStack(app,"GlueDatabase")
GlueCrawlerStack(app,"GlueCrawler")
GlueJobStack(app,"GlueJob")
GlueLambdaStack(app,"GlueLambda")
GlueWorkflowStack(app,"GlueWorkflow")

app.synth()
