import json
import boto3

client = boto3.client(service_name = "glue",
                              region_name = "us-east-1",
                              endpoint_url = "https://glue.us-east-1.amazonaws.com")
                              
def lambda_handler(event,context):
    client.start_workflow_run(Name='glue_workflow')
