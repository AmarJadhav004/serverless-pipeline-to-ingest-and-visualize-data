from aws_cdk import aws_glue as glue
from aws_cdk import core as cdk
import boto3



class GlueCrawlerStack(cdk.Stack):

    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # Creating crawler for raw data
        glue.CfnCrawler(self, "glue_raw_data_crawler_id_5",
                        description= "This Crawler will fetch the schema of raw data",
                        name= "glue_raw_data_crawler_5",
                        database_name= "glue_db_5",
                        schedule= None,
                        role= "arn:aws:iam::316248541916:role/service-role/AWSGlueServiceRole-glue_iam_role",
                        targets= {"s3Targets":[{"path":"s3://project-raw-data-bucket-g25-1/raw_data/"}]})

        # Creating crawler for clean data
        glue.CfnCrawler(self,"glue_clean_data_crawler_id_5",
                        description= "This Crawler will fetch the schema of clean data",
                        name= "glue_clean_data_crawler_5",
                        database_name= "glue_db_5",
                        schedule= None,
                        role= "arn:aws:iam::316248541916:role/service-role/AWSGlueServiceRole-glue_iam_role",
                        targets= {"s3Targets":[{"path":"s3://project-clean-data-bucket-g25-1/clean_data/"}]})                

