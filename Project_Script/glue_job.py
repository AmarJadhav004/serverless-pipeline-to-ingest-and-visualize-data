from aws_cdk import aws_glue as glue
from aws_cdk import core as cdk
from aws_cdk import aws_iam as iam
import boto3



class GlueJobStack(cdk.Stack):

    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        client = boto3.client(service_name = "glue",
                              region_name = "us-east-1",
                              endpoint_url = "https://glue.us-east-1.amazonaws.com")

        
        Job_names = client.list_jobs()["JobNames"]
        if "glue_etl_job" not in Job_names:
            client.create_job(Name = "glue_etl_job",
                            Role='arn:aws:iam::316248541916:role/service-role/AWSGlueServiceRole-glue_iam_role',
                            GlueVersion="2.0",
                            ExecutionProperty={'MaxConcurrentRuns': 2},
                            Command={'Name': 'glueetl','ScriptLocation': 's3://edbda-project-data/data/data_cleaning_script.py'})                   