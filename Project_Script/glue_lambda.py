from aws_cdk import aws_lambda as lb
from aws_cdk import core as cdk
from aws_cdk import aws_iam as iam
from aws_cdk import aws_s3 as s3
from aws_cdk import aws_s3_notifications as s3_notif

class GlueLambdaStack(cdk.Stack):

    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # The code that defines your stack goes here
        lambda_function = lb.Function(self, "lambda_function_id_6",
                                    runtime=lb.Runtime.PYTHON_3_7,
                                    handler="lambdahandler.lambda_handler",
                                    code=lb.Code.asset("./lambda/"),
                                    role=iam.Role.from_role_arn(self, "Role", "arn:aws:iam::316248541916:role/robomaker_students",mutable=False))
        # create s3 bucket
        ip_bucket = s3.Bucket(self,"project-raw-data-bucket-g25-1-id", bucket_name="project-raw-data-bucket-g25-1",public_read_access=True)
        op_bucket = s3.Bucket(self,"project-clean-data-bucket-g25-1-id", bucket_name="project-clean-data-bucket-g25-1")        

        # create s3 notification for lambda function
        notification = s3_notif.LambdaDestination(lambda_function)

        # assign notification for the s3 event type (ex: OBJECT_CREATED)
        ip_bucket.add_event_notification(s3.EventType.OBJECT_CREATED, notification)

