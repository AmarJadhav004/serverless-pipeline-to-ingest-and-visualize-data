from aws_cdk import core as cdk
from aws_cdk import aws_s3 as s3
import boto3


class GlueWorkflowStack(cdk.Stack):

    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        client = boto3.client(service_name = "glue",
                              region_name = "us-east-1",
                              endpoint_url = "https://glue.us-east-1.amazonaws.com")
         
        # Creating glue etl workflow

        workflow_names = client.list_workflows()["Workflows"]

        if "glue_workflow" not in workflow_names:
            client.create_workflow(Name="glue_workflow")

        # Creating raw data crawler trigger
        client.create_trigger(Name="glue_raw_data_crawler_trigger_5",
                            Type='ON_DEMAND',
                            WorkflowName="glue_workflow",                        
                            Actions=[                             
                                    
                                    {   
                                    'Timeout': 123,
                                    'NotificationProperty': {
                                    'NotifyDelayAfter': 123,
                                    },

                                    'CrawlerName': "glue_raw_data_crawler_5"
                                },
                            ]
                        )     

        # Creating glue job trigger
        client.create_trigger(Name="glue_etl_job_trigger",
                            WorkflowName="glue_workflow",
                            Type= 'CONDITIONAL',
                            Predicate={
                                'Logical': 'AND',
                                'Conditions': [
                                    {
                                        'LogicalOperator': 'EQUALS',
                                        'CrawlerName': "glue_raw_data_crawler_5",
                                        'CrawlState': 'SUCCEEDED'
                                    },
                                ]
                            },
                            Actions=[
                                {
                                    'JobName': "glue_etl_job",
                                    'Timeout': 123,
                                    'NotificationProperty': {
                                        'NotifyDelayAfter': 123
                                    },
                                },
                            ],
                            StartOnCreation=True
                        )

        # Creating glue job trigger
        client.create_trigger(Name="glue_clean_data_crawler_trigger_5",
                            WorkflowName="glue_workflow",
                            Type= 'CONDITIONAL',
                            Predicate={
                                'Logical': 'AND',
                                'Conditions': [
                                    {
                                        'LogicalOperator': 'EQUALS',
                                        'JobName': "glue_etl_job",
                                        'State': 'SUCCEEDED',

                                    },
                                ]
                            },
                            Actions=[

                                    {   
                                    'Timeout': 123,
                                    'NotificationProperty': {
                                    'NotifyDelayAfter': 123,
                                    },

                                    'CrawlerName': "glue_clean_data_crawler_5"
                                },
                            ],
                            StartOnCreation=True
                        )                                                        


