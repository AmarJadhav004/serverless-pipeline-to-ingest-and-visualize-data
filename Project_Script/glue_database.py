from aws_cdk import aws_glue as glue
from aws_cdk import core as cdk
import boto3

class GlueDatabaseStack(cdk.Stack):

    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        glue.Database(self,"glue_db_5_id", database_name= "glue_db_5")